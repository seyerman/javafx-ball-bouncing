package threads;

import ui.CircleController;

public class CircleThread extends Thread{
	private CircleController circleController;
	private int circleId;
	private boolean moving;
	
	public CircleThread(CircleController circleC, int id) {
		circleController = circleC;
		circleId = id;
		moving = true;
	}
	
	public void run() {
		while(moving) {
			circleController.moveBlueCircle(circleId);
			try {
				sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setMoving(boolean mov) {
		moving = mov;
	}
	
	public boolean isMoving() {
		return moving;
	}
	
	public int getCircleId() {
		return circleId;
	}
}
