package ui;

import java.io.IOException;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main extends Application{

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("OneBallOnScreen.fxml"));
		Parent root = loader.load();
		Scene scene = new Scene(root);
		
		CircleController cc = loader.getController();
		cc.setStage(stage);
		stage.setOnCloseRequest((new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
            	cc.stopCircles();
            }
        }));
		
		stage.setTitle("Ball Bouncing");
		stage.setScene(scene);
		stage.show();
	}

}
