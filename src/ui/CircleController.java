package ui;

import java.util.ArrayList;
import java.util.List;

import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import threads.CircleThread;

public class CircleController {

    @FXML
    private Pane pane;
    
    private Stage stage;
    
    private List<CircleThread> threadsList;
    
    private enum Direction {RIGHT, LEFT};
    
    private List<Direction> directions;
    
    private List<Circle> circleList;
    
    @FXML
    public void initialize() {
    	threadsList = new ArrayList<CircleThread>();
    	circleList = new ArrayList<>();
    	directions = new ArrayList<>();
    }
   
    @FXML
    void clickOnPane(MouseEvent event) {
    	double x = event.getSceneX();
    	double y = event.getSceneY();
    	
    	Circle c = new Circle(17, Color.rgb((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));
    	c.setLayoutX(x);
    	c.setLayoutY(y);
    	pane.getChildren().add(c);
    	circleList.add(c);
    	
    	directions.add(Direction.RIGHT);
    	
    	CircleThread ct = new CircleThread(this, circleList.size()-1);
    	ct.start();
    	threadsList.add(ct);
    	
    }
    
    public void moveBlueCircle(int id) {
    	Circle c = circleList.get(id);
    	double advance = 10;
    	
    	Direction dir = directions.get(id);
    	
    	switch(dir) {
			case RIGHT:
		    	double remainder = stage.getWidth()-c.getLayoutX()+c.getRadius()*2;
		    	if(remainder<advance) {
		    		advance = remainder;
		    		//System.out.println("c.getLayoutX()="+c.getLayoutX());
		    		//System.out.println("advance="+advance);
		    	}
		    	c.setLayoutX(c.getLayoutX()+advance);
		    	if(c.getLayoutX()+c.getRadius()*2>=stage.getWidth()) {
		    		dir = Direction.LEFT;
		    	}
			break;
			case LEFT:
		    	if(c.getLayoutX()-c.getRadius()<advance) {
		    		advance = c.getLayoutX()-c.getRadius();
		    		//System.out.println("c.getLayoutX()="+c.getLayoutX());
		    		//System.out.println("advance="+advance);
		    	}		    	
		    	c.setLayoutX(c.getLayoutX()-advance);
		    	if(c.getLayoutX()-c.getRadius()<=0) {
		    		dir = Direction.RIGHT;
		    	}
			break;
    	}
    	directions.set(id, dir);
    	String state = "";
    	for(CircleThread ct: threadsList) {
			state += "[id:"+ct.getCircleId()+",mov:"+ct.isMoving()+"] ";
		}
    	System.out.println(state);
    }
    
    public void stopCircles() {
    	System.out.println("Finishing threads...");
    	for(CircleThread ct: threadsList) {
    		ct.setMoving(false);
    	}
    	System.out.println("Threads finished.");
    }
    
    public void setStage(Stage s) {
    	stage = s;
    }
}
